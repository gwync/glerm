## Glerm

Glerm is a puzzle game.  You're under attack by encroaching shapes.  Zap shapes
of the same color to make them vanish, and shapes of another color to swap
colors. If the shape you zap has consecutive shapes of the same color, those
will vanish as well, with score bonuses.

### Installation

Glerm requires Python3 and pygame.

You may simply run ./glerm if you have pygame installed, or pip3 install . from a checkout of this repo.

You may also run pip3 install glerm.

### Controls

Use the arrow keys to move, and fire with the space bar. P pauses, Q quits.

![Screenshot](screenshot.png)
